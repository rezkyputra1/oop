<?php

echo "<h3>output </h3>";
require("frog-ape.php");

$sheep = new animal("shaun");

echo "name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br>"; // "no"
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "name : $sungokong->name <br>"; 
echo "legs : $sungokong->legs <br>"; 
echo "cold blooded : $sungokong->cold_blooded <br>";
echo "yell : $sungokong->yell <br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "name : $kodok->name <br>"; 
echo "legs : $kodok->legs <br>"; 
echo "cold blooded : $kodok->cold_blooded <br>";
echo "jump : $kodok->jump <br>";
$kodok->jump() ; // "hop hop"

?>